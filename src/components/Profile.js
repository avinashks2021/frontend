import { Flex, Text, Button, Link as StyledLink } from "rebass/styled-components"
import { Link } from "react-router-dom"


const Profile = ({department, active, name, id}) => {
    return (
        <Flex p={1} width="100%" alignItems="center">
            <Link component={StyledLink} to={`/update-profile/${id}`} mr="auto">
                {department}
            </Link>
            <Text ml="5">{name}</Text>
            <Text ml="5">5/23/21, 7:43 PM</Text>
            <Text ml="5">5/23/21, 7:43 PM</Text>           
            <Button  ml="5">
                {{active} ? "ACTIVE" : "INACTIVE" }
            </Button>
        </Flex>
    );
}

export default Profile