import React from 'react'
import './App.css'
import { Route, Switch } from 'react-router-dom'
//import Home from './pages/Home'
import Search from './pages/Search'
import { NavBar } from "./components/NavBar"
import FeatureList from './pages/FeatureList'
import Profile from './components/Profile'

function App() {
  return (
    <>
      <NavBar/>
      <Switch>
        <Route path="/update-profile/:id"> 
          <Profile/>
        </Route>
        <Route path="/search"> 
          <Search/>
        </Route>
        <Route path="/" >
          <FeatureList/>
        </Route>
      </Switch>
    </>
  );
}

export default App;
