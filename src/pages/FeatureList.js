import Loader from "react-loader-spinner"
import { useQuery } from "react-query"
import { Flex } from "rebass"
import {getAllFeatures}  from "../API/Api"
import Container from "../components/Container"
import Profile from "../components/Profile"

const FeatureList = () => {
    const data= [{
        "department" : "IT",
        "active" : true,
        "name" : "AVINASH KUMAR SINHA",
        "id" : 1
    },

    {
        "department" : "CSE",
        "active" : true,
        "name" : "RAJIV RANJAN",
        "id" : 2
    }]
   /* const { data, error, isLoading, isError } = useQuery("features", getAllFeatures)
    if(isLoading){
        return (
            <Container py="5" justifyContent="Center">
                <Flex>
                    <Loader type="CradleLoader" color="#ccc" height={40}></Loader>
                </Flex>
            </Container>
        )
    }

    if(isError){
        return <span>Error: {error.message}</span>
    }*/

    return (
        <Container>
            <Flex flexDirection="column" alignItems="center">
                {
                    data.map(({ department, active, name, id}) => (
                        <Profile department={department} active={active} name={name} key={id} id={id}/>
                    ))
                }
            </Flex>
        </Container>
    )
}

export default FeatureList

