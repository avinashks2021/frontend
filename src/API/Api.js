
export const getAllFeatures = async () => {
    const response = await fetch(`/cki/features/readAll`)

    if(!response.ok){
        throw new Error("Something went wrong.")
    }

    return response.json()
}
